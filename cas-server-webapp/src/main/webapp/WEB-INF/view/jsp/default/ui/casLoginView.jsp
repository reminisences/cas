<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <title>单点登录认证系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
		<META name="generator" content="mshtml 11.00.9600.17496">
		
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" media="all" href="<c:url value="/css/style.css"/>"/>
		
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
    </head>

<body>
   <div class="top_div"><!-- id="login" --></div>
   <div class="top_container">
	   <div style="width: 165px; height: 96px; position: absolute;">
	   <div class="tou"></div>
	   <div class="initial_left_hand" id="left_hand"></div>
	   <div class="initial_right_hand" id="right_hand"></div></div>
	        <form:form method="post" id="fm1" commandName="${commandName}" htmlEscape="true" autocomplete="off">
	        	<section>
	            	<c:choose>
		                <c:when test="${not empty sessionScope.openIdLocalId}">
		                    <strong>${sessionScope.openIdLocalId}</strong>
		                    	<input type="hidden" id="username" name="username" value="${sessionScope.openIdLocalId}" />
		                    </P>
		                </c:when>
		                <c:otherwise>
		                    <P style="padding: 30px 0px 10px; position: relative;"><SPAN class="u_logo"></SPAN>
		                    	<form:input id="username" class="ipt" placeholder="请输入用户名" size="25" tabindex="1" accesskey="${userNameAccessKey}" path="username" autocomplete="off" htmlEscape="true" />
		                    </P>
		                </c:otherwise>
	            	</c:choose>
	            </section>
	            
	            <section>
	                <!-- NOTE: Certain browsers will offer the option of caching passwords for a user.  There is a non-standard attribute,
	                "autocomplete" that when set to "off" will tell certain browsers not to prompt to cache credentials.  For more
	                information, see the following web page:
	                http://www.technofundo.com/tech/web/ie_autocomplete.html -->
	                <P style="position: relative;"><SPAN class="p_logo"></SPAN> 
	            		<form:password class="ipt" placeholder="请输入密码" id="password" size="25" tabindex="2" path="password"  accesskey="${passwordAccessKey}" htmlEscape="true" autocomplete="off" />
	            	</P>
	        	</section>
	        	
	        	<section>
	        		<div style="margin-top: 14px;color: red;">
		        		<span>
		        			<form:errors path="*" id="msg" element="div" htmlEscape="false" />
		        		</span>
	        		</div>
	        	</section>
	        	
	        	<section>
	        		<input type="hidden" name="lt" value="${loginTicket}" />
		            <input type="hidden" name="execution" value="${flowExecutionKey}" />
		            <input type="hidden" name="_eventId" value="submit" />
					<!-- <button type="submit" name="submit" accesskey="l" tabindex="4">登录</button> -->
		        	<div style="height: 50px; line-height: 50px; margin-top: 30px; border-top-color: rgb(231, 231, 231); border-top-width: 1px; border-top-style: solid;">
						<p style="margin: 0px 35px 20px 45px;"><span style="float: left;"><a style="color: rgb(204, 204, 204);" href="#">忘记密码?</a></span> 
			           		<span style="float: right;"><a style="color: rgb(204, 204, 204); margin-right: 10px;" href="#">注册</a>
			           		<button type="submit" name="submit" accesskey="l" tabindex="4" style="background: rgb(0, 142, 173); padding: 7px 10px; border-radius: 4px; border: 1px solid rgb(26, 117, 152); border-image: none; color: rgb(255, 255, 255); font-weight: bold;cursor:pointer;">登录</button>  
			           		</span>
		           		</p>
		           	</div>
	           	</section>
       </div>
        	</form:form>
        <!-- <div align="center">Collect from <a href="http://www.cssmoban.com/" target="_blank" title="模板之家">模板之家</a></div> -->
        <!-- Javascript -->
  		<script type="text/javascript" src="<c:url value="/js/jquery-1.9.1.min.js" />"></script>
  		<script type="text/javascript" src="<c:url value="/js/login.js" />"></script>
    </body>
</html>

<%-- <jsp:directive.include file="includes/top.jsp" />
<jsp:directive.include file="includes/bottom.jsp" /> --%>
